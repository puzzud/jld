/** @constructor */
ArkhamTerror.Preloader = function( game )
{
  PuzL.GameScreen.call( this );

  this.preloadBar = null;
  this.titleText = null;
  this.ready = false;
};
extend( ArkhamTerror.Preloader, PuzL.GameScreen );

ArkhamTerror.Preloader.prototype.id = "Preloader";

ArkhamTerror.Preloader.prototype.initialize = function()
{
  this.titleText = this.add.image( this.world.centerX,
                                   this.world.centerY - 220, "titleImage" );
  this.titleText.anchor.setTo( 0.5, 0.5 );
  this.titleText.position.setTo( this.world.centerX,
                                 ( this.world.height * ( 1.0 - 0.618 ) ) | 0 );

  this.preloadBar = this.add.sprite( 0, 0, "preloadBar" );
  this.preloadBar.anchor.setTo( 0.5, 0.5 );
  this.preloadBar.position.setTo( this.world.centerX,
                                  this.world.height - this.preloadBar.height - 32 );
  this.load.setPreloadSprite( this.preloadBar );
  
  this.load.bitmapFont( "arcade_font0", "assets/graphics/fonts/arcade_font0.png", "assets/graphics/fonts/arcade_font0.fnt" );

  //this.load.audio( "explosion_audio", "assets/audio/explosion.mp3" );
  //this.load.audio( "select_audio", "assets/audio/select.mp3" );
  //this.load.audio( "game_audio", "assets/audio/bgm.mp3" );

  this.load.tilemap( "map", "assets/graphics/level0.json", null, Phaser.Tilemap.TILED_JSON );
  this.load.image( "C32 Nukalands", "assets/graphics/tiles/C32 Platformer Tiles_0/Ground/Nukalands.png" );
  this.load.image( "C32 Overworld", "assets/graphics/tiles/C32 Platformer Tiles_0/Ground/Overworld.png" );
  this.load.image( "Ladder",        "assets/graphics/tiles/C32 Platformer Tiles_0/Commons/Ladder.png" );
  
  this.load.atlasJSONArray( "dwarf", "assets/graphics/sprites/dwarf/dwarf_2.png", "assets/graphics/sprites/dwarf/dwarf_2.json" );

  if( window.meSpeak !== undefined )
  {
    meSpeak.loadConfig( "src/mespeak/mespeak_config.json" );
    meSpeak.loadVoice( "src/mespeak/voices/en/en-us.json" );
  }
};

ArkhamTerror.Preloader.prototype.postInitialize = function()
{
  this.preloadBar.cropEnabled = false;
};

ArkhamTerror.Preloader.prototype.update = function()
{
  //if( this.cache.isSoundDecoded( "game_audio" ) && !this.ready )
  {
    this.ready = true;

    this.gameShell.setGameScreen( this.gameShell.startMenuGameScreen );
  }
};
