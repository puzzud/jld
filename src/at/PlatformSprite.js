/** @constructor */
ArkhamTerror.PlatformSprite = function( state )
{
  //PuzL.VideoSprite.call( this );

  this.state = state;
  this.game = state.game;

  this.player = null;

  this.directionButtons = null;

  this.intendedDirection = new Phaser.Point();
  this.facing = -1;
  this.jumpingTimer = 0;
  this.jumping = false;
  this.grounded = false;

  this.movingToGoal = false;
  this.goalRoute = [];
  this.goalRouteTargetIndex = 0;

  this.targetTile = null;
  this.targetPoint = new Phaser.Point();
  this.targetTilePosition = new Phaser.Point();

  this.deviantTargetTile = null;

  this.startPathNodeIndex = -1;
  this.startPathNode = null;
  this.goalPathNodeIndex = -1;
  this.goalPathNode = null;

  this.playerUpPointsMax = 4;
  this.playerUpPoints = this.playerUpPointsMax;

  this.playerTile = null;
  this.playerTilePosition = new Phaser.Point();

  this.startPathNodeIndex = 0;
  this.goalPathNodeIndex = 0;
  if( this.state.pathGraph !== null )
  {
    this.startPathNode = this.state.pathGraph.nodeList[this.startPathNodeIndex];
    this.goalPathNode = this.state.pathGraph.nodeList[this.goalPathNodeIndex];
  }
  
  this.type = 0;

  this.id = ArkhamTerror.PlatformSprite.prototype.instanceCount++;

  /*this.playerPositionText = this.add.bitmapText( 0, 0,
                              "atari_cs",
                              "0\n0",
                              24 );
  //this.playerPositionText.align = "center";

  this.playerPositionText.position.setTo( 0, 0 );
  */

  this.shouldSkipNodes = true;
};
//extend( ArkhamTerror.PlatformSprite, PuzL.VideoSprite );

ArkhamTerror.PlatformSprite.prototype.instanceCount = 0;
ArkhamTerror.PlatformSprite.prototype.textureHash = [];

ArkhamTerror.PlatformSprite.prototype.initialize = function()
{
  var typeString = "";
  switch( this.type )
  {
    case 0:
    default:
    {
      typeString = "dwarf";
    }
  }

  this.player = this.game.add.sprite( 0, 0, typeString );
  
  var texture = null;
  if( texture === null )
  {
    var bmd = this.game.make.bitmapData( this.player.texture.width, this.player.texture.height );
    bmd.load( typeString );

    // TODO: Need to report phaser bug where bmd.load() allows for
    // bmd operations like replaceRGB, whereas no load
    // and sprite bmd.draw() calls do not work. It results
    // in empty images, as various integer buffers are not populated.
    /*var frameList = this.player.animations._frameData._frames;
    var numberOfFrames = frameList.length;

    for( var frameIndex = 0;
         frameIndex < numberOfFrames;
         frameIndex++ )
    {
      this.player.frame = frameIndex;

      var frame = frameList[frameIndex];

      bmd.draw( this.player, frame.x, frame.y );
    }*/

    var red = 128;
    var green = 128;
    var blue = 128;

    var clothesColor = 0x000000;

    if( this.id === 0 )
    {
      clothesColor = ArkhamTerror.prototype.color.get( ArkhamTerror.prototype.color.red );
      //red = 174;
      //green = 0;
      //blue = 0;
    }
    else
    if( this.id === 1 )
    {
      clothesColor = ArkhamTerror.prototype.color.get( ArkhamTerror.prototype.color.green );
      //red = 21;
      //green = 152;
      //blue = 0;
    }
    else
    if( this.id === 2 )
    {
      clothesColor = ArkhamTerror.prototype.color.get( ArkhamTerror.prototype.color.blue );
      //red = 0;
      //green = 108;
      //blue = 164;
    }

    var colorObj = Phaser.Color.getRGB( clothesColor );
    red   = colorObj.red;
    green = colorObj.green;
    blue  = colorObj.blue;

    bmd.replaceRGB( 37, 37, 37, 255, red, green, blue, 255 );

    bmd.replaceRGB( 190, 190, 190, 255, 255, 176, 176, 255 );
    bmd.replaceRGB( 100, 100, 100, 255, 162, 114, 64, 255 );
    bmd.replaceRGB( 255, 255, 255, 255, 255, 255, 255, 255 );
    //bmd.shiftHSL( 1.5 );
    //bmd.processPixelRGB( this.processPixel, this );
    
    texture = bmd.generateTexture( "dwarf" + this.id );
  }

  if( texture !== null )
  {
    this.player.loadTexture( texture );

    ArkhamTerror.PlatformSprite.prototype.textureHash[this.id] = texture;
  }

  this.player.anchor.setTo( 0.5, 0.5 );
  this.player.animations.add( "idle_front", [0], 20, true );
  this.player.animations.add( "idle_right", [6], 20, true );
  this.player.animations.add( "idle_left", [13], 20, true );
  this.player.animations.add( "walk_right", [7, 8, 9, 10, 11, 12], 20, true );
  this.player.animations.add( "walk_left", [14, 15, 16, 17, 18, 19], 20, true );
  this.player.animations.add( "air_front", [1], 20, true );
  this.player.animations.add( "air_right", [21], 20, true );
  this.player.animations.add( "air_left", [23], 20, true );

  this.game.physics.arcade.enable( this.player );
  this.player.body.bounce.y = 0.0;
  this.player.body.gravity.y = 500;
  this.player.body.collideWorldBounds = true;

  this.player.body.fixedRotation = true;

  //this.player.scale.x = 2.0;
  //this.player.scale.y = 2.0;
  this.player.body.setSize( 12, 16, 2, 0 );

  this.player.smoothed = false;
};

ArkhamTerror.PlatformSprite.prototype.bmd = null;

ArkhamTerror.PlatformSprite.prototype.processPixel = function( pixel )
{
  //pixel.r = 255 - pixel.r;
  //pixel.g = 255 - pixel.g;
  //pixel.b = 255 - pixel.b;

  if( ( pixel.r + pixel.g + pixel.b ) > 0 )
  {
    pixel.r = 255;
  }

  return pixel;
}

ArkhamTerror.PlatformSprite.prototype.setGoalTile = function( intendedTile )
{
  if( intendedTile.index > -1 )
  {
    // User selected an impassible tile.
    // NOTE: Perhaps in the future, this would indicate an action of some sort on this tile (eg use or break, etc.)?
    //this.movingToGoal = false;
    return;
  }

  // Find a neareast node tile in direction to intended tile.
  var direction = intendedTile.x - this.playerTile.x;
  var distance  = Math.abs( direction );

  var groundedintendedTile = this.state.getWalkableTileBelow( intendedTile );

  // Determine if intended tile is not a path node.
  // It will need to be moved to after the player has reached the closest node tile (goal).
  var pointerNode = intendedTile.properties.pathNode;

  var goalPathNode = null;
  if( pointerNode === undefined )
  {
    //if( intendedTile.y === this.playerTile.y )
    {
      var groundedPointerNode = groundedintendedTile.properties.pathNode;
      if( groundedPointerNode !== undefined )
      {
        goalPathNode = groundedPointerNode;
      }
      else
      {
        goalPathNode = this.state.pathGraph.getClosestTileNodeHorizontal( groundedintendedTile, -direction, distance );
      }
    }

    this.deviantTargetTile = intendedTile;
  }
  else
  {
    goalPathNode = pointerNode; 
    this.deviantTargetTile = null;
  }

  var groundedPlayerTile = this.state.getWalkableTileBelow( this.playerTile );

  var startPathNode = this.playerTile.properties.pathNode;
  if( startPathNode === undefined )
  {
    startPathNode = groundedPlayerTile.properties.pathNode;
  }
  
  if( startPathNode === undefined )
  {
    startPathNode = null;

    // Player is not currently on a path node.
    if( groundedintendedTile.y === groundedPlayerTile.y )
    {
      startPathNode = this.state.pathGraph.getClosestTileNodeHorizontal( groundedPlayerTile, direction, distance );
    }
  }

  if( ( groundedintendedTile.y === groundedPlayerTile.y ) &&
      ( ( startPathNode === null ) && ( goalPathNode === null ) ) )
  {
    this.goalRoute.length = 0;
    this.goalRouteTargetIndex = -1;

    if( this.deviantTargetTile !== null )
    {
      this.updatePlayerTarget( this.deviantTargetTile );
      this.deviantTargetTile = null;
    }
    
    return false;
  }

  if( ( startPathNode === null ) || ( goalPathNode === null ) )
  {
    // Paths are not obvious. Need to do some multiple route cost estimation.
    var spnList = [];
    var gpnList = [];

    if( direction === 0 )
    {
      direction = 1;
    }

    if( startPathNode !== null )
    {
      spnList.push( startPathNode );
    }
    else
    {
      var spn = this.state.pathGraph.getClosestTileNodeHorizontal( groundedPlayerTile, direction );
      if( spn !== null )
      {
        spnList.push( spn );
      }

      spn = this.state.pathGraph.getClosestTileNodeHorizontal( groundedPlayerTile, -direction );
      if( spn !== null )
      {
        spnList.push( spn );
      }
    }
    
    if( goalPathNode !== null )
    {
      gpnList.push( goalPathNode );
    }
    else
    {
      var gpn = this.state.pathGraph.getClosestTileNodeHorizontal( groundedintendedTile, -direction );
      if( gpn !== null )
      {
        gpnList.push( gpn );
      }
      
      gpn = this.state.pathGraph.getClosestTileNodeHorizontal( groundedintendedTile, direction );
      if( gpn !== null )
      {
        gpnList.push( gpn );
      }
    }

    var route = [];
    var bestRouteCost = Number.MAX_SAFE_INTEGER;
    var bestSpni = 0;
    var bestGpni = 0;
    for( var spni = 0; spni < spnList.length; spni++ )
    {
      route.length = 0;
      for( var gpni = 0; gpni < gpnList.length; gpni++ )
      {
        // Find route and calculate its cost (including any deviation from nodes).
        var rc = this.state.aStar.findPath( spnList[spni], gpnList[gpni], route, this );
        if( rc === Number.MAX_SAFE_INTEGER )
        {
          // This route is not legit.
          continue;
        }

        rc += this.state.tileDistance( this.playerTile, route[0].tile );

        if( this.deviantTargetTile !== null )
        {
          rc += this.state.tileDistance( route[route.length - 1].tile, this.deviantTargetTile );
        }

        if( rc < bestRouteCost )
        {
          bestRouteCost = rc;
          bestSpni = spni;
          bestGpni = gpni;
        }
      }
    }

    startPathNode = spnList[bestSpni];
    goalPathNode = gpnList[bestGpni];
  }

  if( startPathNode !== null )
  {
    //if( startPathNode.tile !== undefined )
    {
      this.targetTile = startPathNode.tile;
    }
    /*else
    {
      this.targetTile = null;
    }*/
  }

  this.startPathNode = startPathNode;
  this.goalPathNode = goalPathNode;
  this.state.aStar.findPath( startPathNode, goalPathNode, this.goalRoute, this );
  this.goalRouteTargetIndex = 0;

  //this.printNodePath( this.goalRoute );

  this.updatePlayerTarget( this.targetTile );

  this.announceMovement();

  return true;
};

ArkhamTerror.PlatformSprite.prototype.updatePlayerTarget = function( targetTile )
{ 
  // Target tile is not associated with a path node.
  this.targetTile = targetTile;

  this.targetTilePosition.x = targetTile.x;
  this.targetTilePosition.y = targetTile.y;

  var tileWidth  = this.state.map.tileWidth;
  var tileHeight = this.state.map.tileHeight;
  var tileHalfWidth = tileWidth >> 1;
  var tileHalfHeight = tileHeight >> 1;

  this.targetPoint.x = ( targetTile.x * tileWidth )  + tileHalfWidth;
  this.targetPoint.y = ( targetTile.y * tileHeight ) + tileHalfHeight;

  this.movingToGoal = true;

  // Get player avatar's tile position.

  //this.boomSound.play();
  //this.boomSound.volume = 0.2;
};

ArkhamTerror.PlatformSprite.prototype.update = function()
{
  // Get player tile position.
  var tileWidth  = this.state.map.tileWidth;
  var tileHeight = this.state.map.tileHeight;
    
  this.playerTilePosition.x = ( this.player.x / tileWidth ) | 0;
  this.playerTilePosition.y = ( this.player.y / tileHeight ) | 0;

  this.playerTile = this.state.layer.layer.data[this.playerTilePosition.y][this.playerTilePosition.x];
  
  //this.playerPositionText.text = this.player.x + "\n" + this.player.y;

  this.intendedDirection.x = 0;
  this.intendedDirection.y = 0;

  // Get intended direction based on user input.
  if( this.directionButtons !== null )
  {
    this.intendedDirection.x += this.directionButtons.left.isDown  ? -1 : 0;
    this.intendedDirection.x += this.directionButtons.right.isDown ?  1 : 0;

    this.intendedDirection.y += this.directionButtons.up.isDown    ? -1 : 0;
    this.intendedDirection.y += this.directionButtons.isDown       ?  1 : 0;
  }
  
  if( this.state.gamepad.connected )
  {
    if( this.state.gamepad.isDown( Phaser.Gamepad.XBOX360_DPAD_LEFT ) ||
        this.state.gamepad.axis( Phaser.Gamepad.XBOX360_STICK_LEFT_X ) < -0.1 )
    {
      this.intendedDirection.x--;
    }
    else
    if( this.state.gamepad.isDown( Phaser.Gamepad.XBOX360_DPAD_RIGHT ) ||
        this.state.gamepad.axis( Phaser.Gamepad.XBOX360_STICK_LEFT_X ) > 0.1 )
    {
      this.intendedDirection.x++;
    }

    if( this.state.gamepad.isDown( Phaser.Gamepad.XBOX360_DPAD_UP ) ||
        this.state.gamepad.axis( Phaser.Gamepad.XBOX360_STICK_LEFT_Y ) < -0.1 )
    {
      this.intendedDirection.y--;
    }
    else
    if( this.state.gamepad.isDown( Phaser.Gamepad.XBOX360_DPAD_DOWN ) ||
        this.state.gamepad.axis( Phaser.Gamepad.XBOX360_STICK_LEFT_Y ) > 0.1 )
    {
      this.intendedDirection.y++;
    }
  }

  if( ( this.intendedDirection.x !== 0.0 ) ||
       ( this.intendedDirection.y !== 0.0 ) )
  {
    // Player input has interrupted AI.
    this.movingToGoal = false;
    this.goalRoute.length = 0;
    this.goalPathNode = null;
  }

  if( this.movingToGoal )
  {
    // Get intended direction based on AI.
    
    // Stop moving to goal if position is in same tile.
    if( Math.abs( this.targetPoint.x - this.player.x ) < 6 )
    //if( this.playerTilePosition.x === this.targetTilePosition.x )
    {
      this.intendedDirection.x = 0;
    }
    else
    {
      this.intendedDirection.x += this.targetPoint.x - this.player.x;
      //this.intendedDirection.x += this.targetTilePosition.x - this.playerTilePosition.x;
    }

    if( this.player.body.blocked.down )
    {
      var angle = this.game.math.angleBetween( this.playerTile.x, this.playerTile.y,
                                               this.targetTile.x, this.targetTile.y );
      angle = this.game.math.radToDeg( angle );
      angle = Math.abs( angle );
      if( angle >= 60.0 && angle <= 120.0 )
      {
        this.intendedDirection.y += this.targetPoint.y - this.player.y;
      }
    }
    else
    {
      this.intendedDirection.y += this.targetPoint.y - this.player.y;
    }

    if( this.playerTile === this.targetTile )
    //( ( this.intendedDirection.x === 0 ) && ( this.intendedDirection.y === 0 ) )
    {
      if( ++this.goalRouteTargetIndex < this.goalRoute.length )
      {
        this.targetTile = this.goalRoute[this.goalRouteTargetIndex].tile;

        // Evaluate ascending.
        /*var ri = this.goalRouteTargetIndex;
        var currentTile = this.playerTile;
        var nextTile = this.targetTile;

        var ascendPoints = this.playerUpPointsMax;

        while( ri < this.goalRoute.length )
        {
          nextTile = this.goalRoute[ri].tile;
          if( nextTile.properties.pathNode.type !== PuzL.PlatformPathGraphNode.prototype.TYPE_DROP )
          {
            break;
          }

          ascendPoints -= this.state.tileDistance( currentTile, nextTile );
          if( ascendPoints <= 0 )
          {
            break;
          }

          ri++;
          currentTile = nextTile;
        }

        if( ascendPoints < 0 )
        {
          // Entity cannot get to height with this path.
          // Add the last offending node to the closed
          // list for path finding.
          this.state.aStar.closedList.push( nextTile.properties.pathNode );

          var goalTile = this.goalPathNode.tile;
          this.setGoalTile( goalTile );
        }
        else
        {
          this.updatePlayerTarget( this.targetTile );
        }*/

        if( this.shouldSkipNodes )
        {
          this.targetTile = this.skipPathNodes();
        }

        {
          this.updatePlayerTarget( this.targetTile );
        }
      }
      else
      {
        if( this.deviantTargetTile === null )
        {
          this.movingToGoal = false;
          this.goalRoute.length = 0;
          this.goalPathNode = null;
        }
        else
        if( this.deviantTargetTile === this.playerTile )
        {
          this.deviantTargetTile = null;
          this.movingToGoal = false;
          this.goalRoute.length = 0;
          this.goalPathNode = null;
        }
        else
        {
          this.updatePlayerTarget( this.deviantTargetTile );
        }
      }
    }
    else
    { 
      if( this.shouldSkipNodes && !this.state.pathGraph.doesDirectPathExist( this.playerTile, this.targetTile ) )
      {
        var gt = ( this.deviantTargetTile === null ) ? this.goalPathNode.tile : this.deviantTargetTile;
        this.setGoalTile( gt );
      }
    }
  }

  // Handle vertical direction / movemet.
  if( this.jumpingTimer-- > 0 )
  {
    this.player.body.velocity.y = -125;

    this.grounded = false;
  }

  //if( ( this.intendedDirection.y >= 0 ) || this.player.body.blocked.down )
  if( this.player.body.blocked.down )
  {
    this.grounded = true;
    this.jumping = false;

    this.jumpingTimer = 0;

    if( this.intendedDirection.y < 0 )
    {
      this.jumpingTimer = 10;
      this.jumping = true;
    }
  }

  // Handle horizontal direction / movemet.
  if( this.intendedDirection.x < 0 )
  {
    this.player.body.velocity.x = -100;
  }
  else
  if( this.intendedDirection.x > 0 )
  {
    this.player.body.velocity.x = 100;
  }
  else
  {
    this.player.body.velocity.x = 0;
  }

  if( this.player.body.blocked.left || this.player.body.blocked.right )
  {
    this.player.animations.stop();

    if( !this.jumping )
    {
      if( this.facing === -1 )
      {
        this.player.animations.play( "idle_left" );
      }
      else
      if( this.facing === 1 )
      {
        this.player.animations.play( "idle_right" );
      }
      else
      {
        this.player.animations.play( "idle_front" );
      }

      return;
    }
  }

  if( !this.jumping )
  {
    // Not jumping.
    if( this.player.body.velocity.x < 0 )
    {
      //if( this.facing !== -1 )
      {
        this.facing = -1;

        //this.player.animations.stop();
        this.player.animations.play( "walk_left" );
      }
    }
    else
    if( this.player.body.velocity.x > 0 )
    {
      //if( this.facing !== 1  )
      {
        this.facing = 1;

        //this.player.animations.stop();
        this.player.animations.play( "walk_right" );
      }
    }
    else
    {
      if( this.facing == -1 )
      {
        //this.player.animations.stop();
        this.player.animations.play( "idle_left" );
      }
      else
      if( this.facing == 1 )
      {
        //this.player.animations.stop();
        this.player.animations.play( "idle_right" );
      }
      else
      //if( this.facing == 0 )
      {
        //this.player.animations.stop();
        this.player.animations.play( "idle_front" );
      }
    }
  }
  else
  {
    // Jumping.
    if( this.player.body.velocity.x < 0 )
    {
      //if( this.facing !== -1 )
      {
        //this.facing = -1;

        //this.player.animations.stop();
        this.player.animations.play( "air_left" );
      }
    }
    else
    if( this.player.body.velocity.x > 0 )
    {
      //if( this.facing !== 1  )
      {
        //this.facing = 1;

        //this.player.animations.stop();
        this.player.animations.play( "air_right" );
      }
    }
    else
    {
      //if( this.facing !== 0 )
      {
        this.facing = 0;
        
        //this.player.animations.stop();
        this.player.animations.play( "air_front" );
      }
    }
  }
};

// Returns tile that can be skipped to (eg new target tile).
ArkhamTerror.PlatformSprite.prototype.skipPathNodes = function()
{
  var nextTargetTile = this.targetTile;

  var nextTile = this.targetTile;
  var nextTileNode = null;

  var nextRi = this.goalRouteTargetIndex;

  var ri = nextRi;
  while( ri < this.goalRoute.length )
  {
    nextTile = this.goalRoute[ri].tile;
    nextTileNode = nextTile.properties.pathNode;
    if( nextTileNode !== undefined )
    {
      // TODO: This is temporary until a sprite's current & potential jump velocity can be
      // factored in to determine if a drop node can be reached.
      if( nextTileNode.type === PuzL.PlatformPathGraphNode.prototype.TYPE_DROP )
      {
        //if( this.state.tileDistance( this.playerTile, nextTile ) > this.playerUpPointsMax )
        {
          ri++;
          continue;
        }
      }
    }

    if( this.state.pathGraph.doesDirectPathExist( this.playerTile, nextTile ) )
    {
      nextRi = ri;
      nextTargetTile = nextTile;
    }

    ri++;
  }

  // Perhaps check for deviant target tile.
  if( ri === this.goalRoute.length )
  {
    if( this.deviantTargetTile !== null )
    {
      if( this.state.pathGraph.doesDirectPathExist( this.playerTile, this.deviantTargetTile ) )
      {
        nextRi = ri;
        nextTargetTile = this.deviantTargetTile;
      }
    }
  }

  this.goalRouteTargetIndex = nextRi;
  return nextTargetTile;
};

ArkhamTerror.PlatformSprite.prototype.aStarPathLogic = function( node0, node1, weight )
{
  /*
  PuzL.PlatformPathGraphNode.prototype.TYPE_NONE = 0;
  PuzL.PlatformPathGraphNode.prototype.TYPE_WALK = 1;
  PuzL.PlatformPathGraphNode.prototype.TYPE_DROP = 2;
  PuzL.PlatformPathGraphNode.prototype.TYPE_LAND = 3;
  */

  var node0Type = node0.type;
  var node1Type = node1.type;

  var verticalTileDistance = node1.tile.y - node0.tile.y;
  if( verticalTileDistance >= 0 )
  {
    // Player is going to drop down.
    return weight;
  }

  if( node1Type === PuzL.PlatformPathGraphNode.prototype.TYPE_DROP )
  {
    // Player is going to jump.

    verticalTileDistance = Math.abs( verticalTileDistance );
    if( verticalTileDistance > this.playerUpPointsMax )
    {
      return Number.MAX_SAFE_INTEGER;
    }
    
    var tileDistance = this.state.tileDistance( node0.tile, node1.tile );

    weight *= tileDistance * 4;

    if( node0Type === PuzL.PlatformPathGraphNode.prototype.TYPE_DROP )
    {
       weight *= 2;
    }
  }

  return weight;
};

ArkhamTerror.PlatformSprite.prototype.announceMovement = function()
{
  if( window.meSpeak !== undefined )
  {
    var phrase = "Moving to node" + " " + this.goalPathNode.id;
    //meSpeak.loadVoice("voices/fr.json");
    //meSpeak.speak('hello world');
    //meSpeak.speak('hello world', { option1: value1, option2: value2 .. });
    //meSpeak.speak('hello world', { option1: value1, option2: value2 .. }, myCallback);
    //var id = meSpeak.speak(  );
    meSpeak.speak( phrase,
      {
        amplitude: 100,
        wordgap: 0,
        pitch: 50,
        speed: 175,
        variant: "whisperf"
      }
    );
    //meSpeak.stop(id);
  }
};

ArkhamTerror.PlatformSprite.prototype.render = function()
{
  var tileWidth  = this.state.map.tileWidth;
  var tileHeight = this.state.map.tileHeight;

  var goalRouteLength = this.goalRoute.length;
  if( goalRouteLength > 0 )
  {
    var tileHalfWidth = tileWidth >> 1;
    var tileHalfHeight = tileHeight >> 1;

    this.game.debug.context.strokeStyle = "#0f0";

    this.game.debug.context.beginPath();

    var node = this.goalRoute[0];
    this.game.debug.context.moveTo( node.sx + tileHalfWidth, node.sy + tileHalfHeight );

    for( var j = 1; j < goalRouteLength; j++ )
    {
      node = this.goalRoute[j];

      this.game.debug.context.lineTo( node.sx + tileHalfWidth, node.sy + tileHalfHeight );
    }

    this.game.debug.context.stroke();
  }

  if( this.goalPathNode !== null )
  {
    if( this.targetNode !== this.startPathNode )
    {
      this.game.debug.context.strokeStyle = "#0f0";
      this.game.debug.context.strokeRect( this.startPathNode.sx, this.startPathNode.sy, tileWidth, tileHeight );
    }
    
    this.game.debug.context.strokeStyle = "#ff0";
    //this.renderNodeConnections( this.goalPathNode );
    this.game.debug.context.strokeRect( this.goalPathNode.sx, this.goalPathNode.sy, tileWidth, tileHeight );
  }

  if( this.movingToGoal )
  {
    this.game.debug.context.strokeStyle = "red";
    this.game.debug.context.beginPath();

    this.game.debug.context.moveTo( this.player.x, this.player.y );
    this.game.debug.context.lineTo( this.targetPoint.x, this.targetPoint.y );

    this.game.debug.context.stroke();

    this.game.debug.context.strokeRect( this.targetTilePosition.x * tileWidth, this.targetTilePosition.y * tileHeight, tileWidth, tileHeight );

    if( this.deviantTargetTile !== null && this.targetTile !== this.deviantTargetTile )
    {
      this.game.debug.context.strokeStyle = "orange";
      this.game.debug.context.strokeRect( this.deviantTargetTile.x * tileWidth, this.deviantTargetTile.y * tileHeight, tileWidth, tileHeight );
    }
  }

  this.game.debug.context.strokeStyle = "white";
  this.game.debug.context.strokeRect( this.playerTilePosition.x * tileWidth, this.playerTilePosition.y * tileHeight, tileWidth, tileHeight );

  this.game.debug.context.font = "32px Georgia";
  this.game.debug.context.fillStyle = "white";
  this.game.debug.context.fillText( "Grounded: " + this.grounded, 0, 32 );
  this.game.debug.context.fillText( "Jumping: " + this.jumping, 0, 64 );
};

ArkhamTerror.PlatformSprite.prototype.printNodePath = function( route )
{
  var routeString = "";

  var routeLength = route.length;
  for( var i = 0; i < routeLength; i++ )
  {
    if( i > 0 )
    {
      routeString += ",";
    }

    routeString += route[i].id;
  }

  console.log( routeString );
};
