/** @constructor */
ArkhamTerror.Game = function( game )
{
  PuzL.GameScreen.call( this );

  this.playerSpriteList = new Array();
  
  this.activePlayerSprite = null;

  this.cursors = null;
  this.wasd = null;
  this.spaceBar = null;

  this.debugModeKey = null;
  this.debugMode = false;

  this.gamepad = null;

  this.map = null;
  this.layer = null;

  this.aStar = null;
  this.pathGraph = null;

  this.musicSound = null;
  this.ouchSound = null;
  this.boomSound = null;
  this.endSound = null;

  this.playerPositionText = null;
};
extend( ArkhamTerror.Game, PuzL.GameScreen );

ArkhamTerror.Game.prototype.id = "Game";

ArkhamTerror.Game.prototype.postInitialize = function()
{
  this.input.onDown.add( this.pointerDown, this );

  //this.camera.follow( this.player );

  this.cursors = this.input.keyboard.createCursorKeys();
  this.spaceBar = this.input.keyboard.addKey( Phaser.Keyboard.SPACEBAR );
  this.debugModeKey = this.input.keyboard.addKey( Phaser.Keyboard.R );

  this.wasd =
  {
    up:    this.game.input.keyboard.addKey( Phaser.Keyboard.W ),
    down:  this.game.input.keyboard.addKey( Phaser.Keyboard.S ),
    left:  this.game.input.keyboard.addKey( Phaser.Keyboard.A ),
    right: this.game.input.keyboard.addKey( Phaser.Keyboard.D ),
  };

  this.spaceBar.onDown.add( this.spaceBarDown, this );
  this.debugModeKey.onDown.add( this.debugModeKeyDown, this );

  this.gameShell.initializeGamepads();
  this.gamepad = this.game.input.gamepad.pad1;

  this.buildWorld();

  this.activePlayerSprite = this.playerSpriteList[0];

  //this.musicSound = this.add.audio( "game_audio" );
  //this.musicSound.play( "", 0, 0.3, true );
  //this.ouchSound = this.add.audio( "hurt_audio" );
  //this.boomSound = this.add.audio( "explosion_audio" );
  //this.endSound = this.add.audio( "select_audio" );
};

ArkhamTerror.Game.prototype.buildWorld = function()
{
  this.physics.startSystem( Phaser.Physics.ARCADE );

  this.map = this.game.add.tilemap( "map" );
  this.map.addTilesetImage( "C32 Nukalands" );
  this.map.addTilesetImage( "C32 Overworld" );
  this.map.addTilesetImage( "Ladder" );
  this.map.smoothed = false;

  this.layer = this.map.createLayer( "Platforms" );
  this.map.createLayer( "Decorations" );
  this.layer.resizeWorld();

  // Build collision list.
  // TODO: Pull tileset names from list rather than hardcode.
  var collisionTileIndexList = [];
  var tilesets = this.map.tilesets;
  var numberOfTilesets = tilesets.length;
  var tileIndex = 0;

  for( var i = 0; i < numberOfTilesets; i++ )
  {
    var tileset = tilesets[i];
    //if( ( tileset.name === "C32 Nukalands" ) ||
    //    ( tileset.name === "C32 Overworld" ) )
    {
      for( var n = 0; n < tileset.total; n++ )
      {
        var tileProperty = tileset.tileProperties[n];
        if( tileProperty !== undefined &&
            tileProperty.block !== undefined &&
            tileProperty.block !== 0 )
        {
          collisionTileIndexList.push( tileIndex );
        }

        tileIndex++;
      }
    }
  }

  this.map.setCollision( collisionTileIndexList, true, this.layer );

  this.pathGraph = new PuzL.PlatformPathGraph( this.layer );
  this.aStar = new PuzL.AStar( this.pathGraph );

  var playerSprite = null;
  for( var i = 0; i < 3; i++ )
  {
    playerSprite = new ArkhamTerror.PlatformSprite( this );
    playerSprite.type = i;
    playerSprite.initialize();

    playerSprite.player.position.set( ( 64 * i ) + 64, 128 );

    if( i === 0 )
    {
      playerSprite.directionButtons = this.wasd;
      playerSprite.shouldSkipNodes = true;
    }
    else
    if( i === 1 )
    {
      playerSprite.directionButtons = this.cursors;
      playerSprite.shouldSkipNodes = false;
    }

    playerSprite.smoothed = false;

    this.playerSpriteList.push( playerSprite );
  }
};

ArkhamTerror.Game.prototype.spaceBarDown = function( button )
{
  var currentActivePlayerIndex = this.playerSpriteList.indexOf( this.activePlayerSprite );
  if( currentActivePlayerIndex < 0 )
  {
    return;
  }

  if( ++currentActivePlayerIndex >= this.playerSpriteList.length )
  {
    currentActivePlayerIndex = 0;
  }

  this.activePlayerSprite = this.playerSpriteList[currentActivePlayerIndex];
};

ArkhamTerror.Game.prototype.debugModeKeyDown = function( button )
{
  this.debugMode = !this.debugMode;
};

ArkhamTerror.Game.prototype.pointerDown = function( pointer )
{
  // Determine start tile from player location.
  var layerData = this.layer.layer.data;

  // Determine goal tile from goal point.
  var tileWidth  = this.map.tileWidth;
  var tileHeight = this.map.tileHeight;

  var pointerTileX = ( pointer.x / tileWidth )  | 0;
  var pointerTileY = ( pointer.y / tileHeight ) | 0;
  
  var pointerTile = layerData[pointerTileY][pointerTileX];//this.map.getTile( pointerTileX, pointerTileY, this.layer );

  if( true )
  {
    this.activePlayerSprite.setGoalTile( pointerTile );
  }
  else
  {
    var playerSprite = null;
    for( var i = 0; i < this.playerSpriteList.length; i++ )
    {
      playerSprite = this.playerSpriteList[i];
      playerSprite.setGoalTile( pointerTile );
    }
  }

  /*
  var pointerNode = pointerTile.properties.pathNode;
  if( pointerNode !== undefined )
  {
    var nodeList = pointerNode.nodeList;
    var nodeListLength = nodeList.length;
    if( nodeListLength > 0 )
    {
      var nodeListString = "";
      for( var i = 0; i < nodeListLength; i++ )
      {
        if( i > 0 )
        {
          nodeListString += ",";
        }

        nodeListString += nodeList[i].id;
      }

      console.log( "Adjacent nodes to " + pointerNode.id + ":  " + nodeListString );
    }
    else
    {
      console.log( "No adjacent nodes to " + pointerNode.id );
    }

  }
  */
};

ArkhamTerror.Game.prototype.getWalkableTileBelow = function( tile )
{
  var layerData = this.layer.layer.data;

  var nextTile = null;

  var height = this.layer.width;

  var x = tile.x;
  var y = tile.y - 1;

  while( ++y < height )
  {
    nextTile = layerData[y][x];
    if( nextTile === undefined )
    {
      return null;
    }

    if( nextTile.index > -1 )
    {
      return tile;
    }

    tile = nextTile;
  }

  return null;
};

ArkhamTerror.Game.prototype.update = function()
{
  var playerSprite = null;

  for( var i = 0; i < this.playerSpriteList.length; i++ )
  {
    playerSprite = this.playerSpriteList[i];

    this.physics.arcade.collide( playerSprite.player, this.layer );
  }

  for( var i = 0; i < this.playerSpriteList.length; i++ )
  {
    playerSprite = this.playerSpriteList[i];
    
    playerSprite.update();
  }
};

ArkhamTerror.Game.prototype.tileDistance = function( tile1, tile0 )
{
  // Manhattan method.
  return Math.abs( tile1.x - tile0.x ) + Math.abs( tile1.y - tile0.y );  
};

ArkhamTerror.Game.prototype.renderNodeConnections = function( node )
{
  // Draw a line from this node to all its connecting nodes.
  var tileWidth  = this.map.tileWidth;
  var tileHeight = this.map.tileHeight;
  var tileHalfWidth  = tileWidth >> 1;
  var tileHalfHeight = tileHeight >> 1;

  var numberOfConnections = node.nodeList.length;
  for( var j = 0; j < numberOfConnections; j++ )
  {
    var connectedNode = node.nodeList[j];
    this.game.debug.context.beginPath();
    this.game.debug.context.moveTo( node.sx + tileHalfWidth , node.sy + tileHalfHeight );
    this.game.debug.context.lineTo( connectedNode.sx + tileHalfWidth, connectedNode.sy + tileHalfHeight );
    this.game.debug.context.stroke();
  }
};

ArkhamTerror.Game.prototype.render = function()
{
  if( !this.debugMode )
  {
    return;
  }

  if( this.pathGraph !== null )
  {
    this.game.debug.context.strokeStyle = "#333";

    var node = null;

    var numberOfGraphNodes = this.pathGraph.nodeList.length;
    for( var i = 0; i < numberOfGraphNodes; i++ )
    {
      node = this.pathGraph.nodeList[i];

      this.renderNode( node );

      this.renderNodeConnections( node );
    }
  }

  if( this.activePlayerSprite !== null )
  {
    this.activePlayerSprite.render();
  }
  /*
  for( var i = 0; i < this.playerSpriteList.length; i++ )
  {
    this.playerSpriteList[i].render();
  }
  */
};

ArkhamTerror.Game.prototype.renderNode = function( node )
{
  var colorBackup = this.game.debug.context.strokeStyle;

  var typeColor = "";

  switch( node.type )
  {
    case PuzL.PlatformPathGraphNode.prototype.TYPE_WALK:
    {
      typeColor = "brown";
      break;
    }

    case PuzL.PlatformPathGraphNode.prototype.TYPE_DROP:
    {
      typeColor = "teal";
      break;
    }

    case PuzL.PlatformPathGraphNode.prototype.TYPE_LAND:
    {
      typeColor = "gold";
      break;
    }

    default:
    {
      typeColor = "cyan";
    }
  }

  var tileWidth  = this.map.tileWidth;
  var tileHeight = this.map.tileHeight;
  var tileHalfWidth  = tileWidth >> 1;
  var tileHalfHeight = tileHeight >> 1;
  var tileQuarterWidth  = tileHalfWidth >> 1;
  var tileQuarterHeight = tileHalfHeight >> 1;

  this.game.debug.context.strokeStyle = typeColor;
  this.game.debug.context.strokeRect( ( node.x * tileWidth ) + ( 1.5 * tileQuarterWidth ),
                                      ( node.y * tileHeight ) + ( 1.5 * tileQuarterHeight ),
                                      tileQuarterWidth, tileQuarterHeight );
  
  this.game.debug.context.strokeStyle = colorBackup;
  this.game.debug.context.strokeRect( node.x * tileWidth,
                                      node.y * tileHeight,
                                      tileWidth, tileHeight );

  this.game.debug.context.font = "10px Georgia";
  this.game.debug.context.fillStyle = "white";
  this.game.debug.context.fillText( node.id,
                                   ( node.x * tileWidth ) + 4,
                                   ( node.y * tileHeight ) + 4 );
};

