/** @constructor */
ArkhamTerror.Boot = function( game )
{
  PuzL.GameScreen.call( this );
};
extend( ArkhamTerror.Boot, PuzL.GameScreen );

ArkhamTerror.Boot.prototype.id = "Boot";

ArkhamTerror.Boot.prototype.initialize = function()
{
  this.load.image( "preloadBar", "assets/graphics/loader_bar.png" );
  this.load.image( "titleImage", "assets/graphics/TitleImage.png" );
};

ArkhamTerror.Boot.prototype.postInitialize = function()
{
  this.input.maxPointers = 1;
  this.stage.disableVisibilityChange = false;
  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  this.scale.minWidth = 480;
  this.scale.minHeight = 270;
  this.scale.pageAlignHorizontally = true;
  this.scale.pageAlignVertically = true;
  this.stage.forcePortrait = false;
  this.scale.refresh();

  this.input.addPointer();
  this.stage.backgroundColor = "#171642";

  this.gameShell.setGameScreen( this.gameShell.preloaderGameScreen );
};

ArkhamTerror.Boot.prototype.update = function()
{
  
};
