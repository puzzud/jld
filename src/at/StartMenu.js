/** @constructor */
ArkhamTerror.StartMenu = function( game )
{
  PuzL.GameScreen.call( this );

  this.titleText = null;

  this.startPrompt = null;

  this.startSound = null;
};
extend( ArkhamTerror.StartMenu, PuzL.GameScreen );

ArkhamTerror.StartMenu.prototype.id = "StartMenu";

ArkhamTerror.StartMenu.prototype.postInitialize = function()
{
  this.titleText = this.add.image( this.world.centerX,
                                   this.world.centerY - 220, "titleImage" );
  this.titleText.anchor.setTo( 0.5, 0.5 );
  this.titleText.position.setTo( this.world.centerX,
                                 ( this.world.height * ( 1.0 - 0.618 ) ) | 0 );

  this.startPrompt = this.add.bitmapText( 0, 0,
                                          "arcade_font0",
                                          "TOUCH TO START!",
                                          16 );
  this.startPrompt.align = "center";

  this.startPrompt.position.setTo( this.world.centerX - ( ( this.startPrompt.width / 2 ) | 0 ),
                                   this.world.height - 144 );

  this.startPrompt.smoothed = false;

  this.input.onDown.addOnce( this.startGame, this );
  
  //this.startSound = this.add.audio( "select_audio" );
};

ArkhamTerror.StartMenu.prototype.startGame = function()
{
  //this.startSound.play();
  
  this.gameShell.setGameScreen( this.gameShell.gameGameScreen );
};
