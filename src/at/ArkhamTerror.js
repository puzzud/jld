/** @constructor */
var ArkhamTerror = function()
{
  PuzL.GameShell.call( this );

  this.bootGameScreen      = null;
  this.preloaderGameScreen = null;
  this.startMenuGameScreen = null;
  this.gameGameScreen      = null;
};
extend( ArkhamTerror, PuzL.GameShell );
window['ArkhamTerror'] = ArkhamTerror;

ArkhamTerror.prototype.gameShellSettings =
{
  //width: 960,
  //height: 540,

  width: 256,
  height: 240,

  pixelPerfect: true,

  pauseOnLoseFocus: false,

  enableGamepads: true
};

ArkhamTerror.prototype.color =
{
  palette:
  [
    0x000000, // black
    0xff0000, // red
    0xde9751, // brown
    0xffb8ff, // purple
    0x00ffff, // cyan
    0x47b8ff, // blue_light
    0xffb851, // orange
    0xffff00, // yellow
    0x2121ff, // blue
    0x00ff00, // green
    0x47b8ae, // teal
    0xffb8ae, // pink
    0xdedeff  // white
  ],
  black: 0,
  red: 1,
  brown: 2,
  purple: 3,
  cyan: 4,
  blue_light: 5,
  orange: 6,
  yellow: 7,
  blue: 8,
  green: 9,
  teal: 10,
  pink: 11,
  white: 12,
  get: function( colorId )
  {
    if( colorId < 0 || colorId > this.palette.length )
    {
      colorId = this.black;
    }

    return this.palette[colorId];
  }
};

PuzL.GameShell.prototype.initialize = function()
{
  this.bootGameScreen      = this.addGameScreen( ArkhamTerror.Boot );
  this.preloaderGameScreen = this.addGameScreen( ArkhamTerror.Preloader );
  this.startMenuGameScreen = this.addGameScreen( ArkhamTerror.StartMenu );
  this.gameGameScreen      = this.addGameScreen( ArkhamTerror.Game );
};

PuzL.GameShell.prototype.postInitialize = function()
{
  this.setGameScreen( this.bootGameScreen );
};
